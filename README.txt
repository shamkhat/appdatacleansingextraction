To install the application as pip package, download the code to folder and run the following command.

pip install -e <fullpath to the folder>

As a backup all the required packages are also available in requirements.txt.


'assoc_overall' script is generated.

Usage is as follows:
'assoc_overall -t <target_identifier>'
'assoc_overall -d <disease_identifier>'
'assoc_overall --test'

Also, the Application can be run directly from cli.py as follows:

src/cli.py -t <target_identifier>
src/cli.py -d <disease_identifier>
src/cli.py --test


The same application can also be implemented using Pandas as single page app. 
 
