""" Entry Point Module for Application
Usage:
  assoc_overall (-t <target>|-d <disease>|--test)
  assoc_overall -h | --help
  cli.py (-t <target>|-d <disease>|--test)
  cli.py -h | --help
"""
from docopt import docopt
from associationscoreoverall import AssociationScoreOverall
from test_associationscoreoverall import main_test

def main_entrypoint():
    cli_args = docopt(__doc__)

    if cli_args.get('-t',None):
        AssociationScoreOverall('target',cli_args['<target>']).display_output()
    if cli_args.get('-d',None):
        AssociationScoreOverall('disease', cli_args['<disease>']).display_output()
    if cli_args.get('--test',None):
	main_test()	


if __name__ == '__main__':
    main_entrypoint()
