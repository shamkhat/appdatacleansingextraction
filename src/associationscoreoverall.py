""" This module tests and validates 
the API functionality of Target Validation.
"""
import requests
import numpy
import logging
import sys

class AssociationScoreOverall(object):
    """ Class Processing AssociationScore Overall
     from Target Validation API.
    """

    def __init__(self, mode, identifier):
        """ Intialise

        :param mode: Requested parameter
        :param identifier: ID of the interested parameter
        """
        self.base_url = 'https://www.targetvalidation.org/api/latest/public/association/filter'

	self._config_logger()

        self.log.debug('Creating Request for %s:%s', mode, identifier)
        self._mode = mode
        self._identifier = identifier

    def _config_logger(self):
        self.log = logging.getLogger(__name__)
	self.log.setLevel(logging.DEBUG)
	_ch = logging.StreamHandler(sys.stdout)
	_ch.setFormatter(logging.Formatter('[%(asctime)s] %(message)s',
					    '%m-%d %H:%M:%S'))
	self.log.addHandler(_ch)
	
    def _fetch_validation_info(self):
        """ Fetch content for requested Parameter and the
        corresponding id.

        :return: Return Dictionary of received info if
        request was successful else return None.
        """
        # Fetch Data from REST API
        resp = requests.get(self.base_url,
                            params={self._mode: self._identifier})

        if resp.status_code != 200:
            self.log.warn('Fetch failed for %s : %s',
                          self._mode, self._identifier)
            return None

        # Convert the received JSON data to dictionary
        return resp.json()

    def _extract_assoc_score_overall(self):
        """ Process response and extract 'overall' value
        from 'association_score'.
        :return: Return Dictionary of received info if request was successful
                else return None.

         Response data has following structure for the interested fields
         {'data': [ {'association_score': {'overall': <float val>} },
                    {'association_score': {'overall': <float val>} }
                  ]
         }
        """
        resp_data = self._fetch_validation_info()
	
        if resp_data:
            # List of Floats
            return map(lambda x: x['association_score']['overall'], resp_data['data'])

        return None

    def _calculate_output(self):
        """ Calculate and return the Min, Max, Average
        and Standard Deviation of the AssociationScore_Overall.

        :return: Tuple of min,max,mean and sd
        """

        as_sc_overall = self._extract_assoc_score_overall()
	
	if as_sc_overall:
        	return (numpy.min(as_sc_overall), 
			numpy.max(as_sc_overall), 
			numpy.mean(as_sc_overall), 
			numpy.std(as_sc_overall))
	return (None,None,None,None)

    def display_output(self):
        """ Fetch and PrintOut
        """
        aso_min, aso_max, aso_mean, aso_sd = self._calculate_output()

        self.log.info('Stats for Association Score Overall for %s - %s :',
                      self._mode, self._identifier)
        self.log.info('Minimum :%s', aso_min)
        self.log.info('Maximum :%s', aso_min)
        self.log.info('Average :%s', aso_min)
        self.log.info('Standard Deviation:%s', aso_min)

