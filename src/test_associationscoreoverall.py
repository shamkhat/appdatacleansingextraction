""" Test Functions for verifying the functionality
"""
import unittest
from mock import patch
import associationscoreoverall as aso

def mock_response(*args,**kwargs):
    class MockResp(object):
	def __init__(self,resp_data):
	    self.status_code = 200
	    self.resp_data = resp_data
	def json(self):
	    return self.resp_data

    param = kwargs.pop('params')
    if 'target' in param:
	val_list = [0.004,.01,.1,1.0]
    elif 'disease' in param:
        if 'EFO_0002422' == param['disease']:
	    val_list = [.004,.03,.05,1.0]
	else:
	    val_list = [.004,.06,.08,1.0]
    else:
	val_list = [1.0,1.0,1.0,1.0]

    dict_list = [{'association_score':{'overall':v}} for v in val_list]
    
    return MockResp({'data':dict_list}) 
     
class ASOTestClass(unittest.TestCase):
     
    @patch('associationscoreoverall.requests.get',side_effect=mock_response)
    def test_01_target(self,mock_r_get):
	uut = aso.AssociationScoreOverall('target','ENSG00000157764')._calculate_output()
	assert uut == (0.0040000000000000001, 1.0, 0.27850000000000003, 0.41829027002788388)
 
    @patch('associationscoreoverall.requests.get',side_effect=mock_response)
    def test_02_disease(self,mock_r_get):
	uut = aso.AssociationScoreOverall('disease','EFO_0002422')._calculate_output()
	assert uut == (0.0040000000000000001, 1.0, 0.27100000000000002, 0.42120422599969243)

    @patch('associationscoreoverall.requests.get',side_effect=mock_response)
    def test_03_disease(self,mock_r_get):
	uut = aso.AssociationScoreOverall('disease','EFO_0000616')._calculate_output()
	assert uut == (0.0040000000000000001, 1.0, 0.28600000000000003, 0.41316824660179291)

def main_test():
    unittest.main()

if __name__ == '__main__':
   unittest.main()
