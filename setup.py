""" Setup Module

"""
from setuptools import find_packages
from setuptools import setup

setup(
    name='assc_overall',
    author='Shameem Ahamed',
    description='Calculate Association Score Overall',
    packages=find_packages(),
    install_requires=[
        'setuptools',
        'docopt',
	'requests',
	'mock',
        'numpy',
    ],
    entry_points={
        'console_scripts': [
            'assoc_overall = src.cli:main_entrypoint',
        ],
    },
)
